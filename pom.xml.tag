<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>net.tncy</groupId>
    <artifactId>tncy</artifactId>
    <version>2019.1</version>
  </parent>


  <groupId>net.tncy.marlier11u.bookmanager</groupId>
  <artifactId>book-manager</artifactId>
  <version>0.0.1</version>
  <packaging>pom</packaging>

  <name>book-manager</name>
  <!-- FIXME change it to the project's website -->
  <url>http://www.example.com</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.7</maven.compiler.source>
    <maven.compiler.target>1.7</maven.compiler.target>
  </properties>

  <scm>
    <connection>scm:git:https://bitbucket.org/marlier11u/book_manager</connection>
    <developerConnection>scm:git:https://bitbucket.org/marlier11u/book_manager</developerConnection>
    <url>https://bitbucket.org/marlier11u/book_manager</url>
    <tag>book-manager-0.0.1</tag>
  </scm>

  <modules>
    <module>book_manager_data</module>
    <module>book_manager_service</module>
  </modules>

</project>
